import PyPDF2
from unidecode import unidecode


NOMBRE_A_BUSCAR = input("Nombre a buscar: ")
INICIALES_A_BUSCAR = input("Iniciales a buscar: ")

INPUT_PDF = "BOE-A-2019-9457.pdf"
PAG_ANEXO = 3 # Pagina en la que empieza el ANEXO
NOMBRE_TOTAL_AGENTES = 2587 # Utilizado para hacer un Sanity Check.
                            # Si el script no encuntra este num de agentes es que algo ha ido mal
                            # Haz scroll al final del pdf, el numero que buscas es el "Orden" del ultimo agente


class Agente:
  def __init__(self, nombre, apellidos):
    self.nombre = nombre
    self.apellidos = apellidos

class Results:
    def __init__(self, mismo_nombre, match100, match_2_letras, match100_noname, match_2_letras_noname):
        self.mismo_nombre = mismo_nombre
        self.match100 = match100
        self.match_2_letras = match_2_letras
        self.match100_noname = match100_noname
        self.match_2_letras_noname = match_2_letras_noname




def split_into_lines(text_to_split):
    text_splited = []
    line = ""
    for char in text_to_split:
        if (char == "\n"):
            text_splited.append(line)
            line = ""

        else:
            line += char

    return text_splited

def pdf2names(INPUT_PDF, PAG_ANEXO):
    """Del PDF de entrada extrae los nombres de todos los agentes.
       devuelve una lista de objectos "Agente" """
    lista_agentes = []
    pdfFileObj = open(INPUT_PDF, 'rb')
    pdfReader = PyPDF2.PdfReader(pdfFileObj)

    # Number of pages in pdf file
    LEN_PDF = len(pdfReader.pages)

    #Por cada pagina del ANEXO:
    for n in range(LEN_PDF-PAG_ANEXO+1):
        pageObj = pdfReader.pages[PAG_ANEXO-1+n]
        texto_pag_n = pageObj.extract_text()
        text_splited = split_into_lines(texto_pag_n) #Extrae todo el texto de la pagina

        #Por cada linea de cada pagina, extrae el nombre del agente (si la linea contiene un nombre)
        for line in text_splited:
            line = line.split(',')

            if(len(line) == 3): #Las lineas que contienen los nombres de los agentes tienen 3 comas (,). A si que line ha de tener len == 3
                primera_mitad = line[0].split(" ")
                apellidos = []
                apellidos_str = ""
                for elemento in primera_mitad:
                    if elemento.isalpha():
                        apellidos.append(elemento)

                segunda_mitad = line[1].split(" ")
                nombre = []
                for elemento in segunda_mitad:
                    if elemento.isalpha():
                        nombre.append(elemento)

                for palabra in apellidos:
                    apellidos_str = apellidos_str+palabra+" " #Put all the words of the strings together
                apellidos_str = apellidos_str[:-1] # Remove last extra space
                lista_agentes.append(Agente(nombre[0], apellidos_str))

    # closing the pdf file object
    pdfFileObj.close()
    return lista_agentes

def busca_nombre(nombre, indices):
    """Input: lista de indices. ej;
       Dado un nombre de pila y el output de pdf2names(), muestra el nombre y los apellidos de
       todos los agentes que tengan el mismo nombre"""

    mismo_nombre = []
    for i in indices:
       if(lista_agentes[i].nombre == unidecode(nombre.upper()) or unidecode(nombre.upper()) in lista_agentes[i].nombre):
           mismo_nombre.append(i)

    return mismo_nombre

def busca_iniciales_simple(iniciales, indices):
    """
       Dado unas iniciales y el output de pdf2names(), muestra el nombre y los apellidos de
       todos los agentes que tengan esas inciales
       Las iniciales tienen que ser una string de letras, al estilo de DHP.
       Esta solo funciona con nombres y apellidos simple. Es decir, falla con nombres o apellidos compuestos como "José Luís", o "del Moral"
    """
    match100 = []
    match_2_letras = []

    if len(iniciales) != 3:
       print("Las iniciales han de estar compuestas por 3 letras. ej: JPR\n")
       return 1

    for i in indices:
        nombre = lista_agentes[i].nombre.split(" ")
        apellidos = lista_agentes[i].apellidos.split(" ")

        if ((len(nombre) == 1) and (len(apellidos) == 2)): #Nombres NO compuestos (una palabra para el nombre, 2 para los apellidos)

            # Caso 100% de emparejamiento (ej:ACC --> CARUNCHO CARBALLO, ANGEL)
            if (nombre[0][0] == iniciales[0]) and (apellidos[0][0] == iniciales[1]) and (apellidos[1][0] == iniciales[2]):
                match100.append(i)

            # Caso en que solo la primera y la segunda letra encajan o la primera y la tercera
            elif (nombre[0][0] == iniciales[0] and (apellidos[0][0] == iniciales[1]) or nombre[0][0] == iniciales[0] and (apellidos[1][0] == iniciales[2]) ):
                    match_2_letras.append(i)


    return match100, match_2_letras

def restults_to_file(out_file, resultados):
    with open(out_file, 'w') as f:
        f.write(f"""
=========================================
******* Resultados de la busqueda *******
=========================================

Nombre buscado:     {NOMBRE_A_BUSCAR}
Iniciales buscadas: {INICIALES_A_BUSCAR}

PDF analizado:      {INPUT_PDF}
Nº de agentes buscados: {len(lista_agentes)}
            \n\n""")

        if len(resultados.match100) > 0:
            f.write("Mejores resultados encontrados:\n")
            for i in match100:
                f.write(f"    * {lista_agentes[i].nombre} {lista_agentes[i].apellidos}\n")

        if len(resultados.match_2_letras) > 0:
            f.write("\n\nAgentes con el mismo nombre, y al menos dos iniciales iguales:\n")
            for i in match_2_letras:
                f.write(f"    * {lista_agentes[i].nombre} {lista_agentes[i].apellidos}\n")

        if len(resultados.match100_noname) > 0:
            f.write("\n\nOtros agentes con las mismas iniciales (con un nombre cualquiera):\n")
            for i in match100_noname:
                f.write(f"    * {lista_agentes[i].nombre} {lista_agentes[i].apellidos}\n")



    print(f"Ficha de resultados creada, revisa '{out_file}'")











#Array conteniente de los nombres de los agentes que aparecen en el INPUT_PDF
lista_agentes = pdf2names(INPUT_PDF, PAG_ANEXO)
indices_todos_agentes = []
for i in range(len(lista_agentes)):
    indices_todos_agentes.append(i)

# Revisa que el script haya leido todos los nombres
if len(lista_agentes) != NOMBRE_TOTAL_AGENTES:
  print("\nSanity Check: NOK")
  exit(1)
else:
  print("\nSanity Check: OK")

# Agentes que comparten el nombre con le sospechose i alguna otra inicial
mismo_nombre = busca_nombre(NOMBRE_A_BUSCAR, indices_todos_agentes)
match100, match_2_letras = busca_iniciales_simple(INICIALES_A_BUSCAR, mismo_nombre)

# Agentes que comparten dos o tres iniciales con le sospechose, aun teniendo un nombre distinto
match100_noname, match_2_letras_noname = busca_iniciales_simple(INICIALES_A_BUSCAR, indices_todos_agentes)



resultados = Results(mismo_nombre, match100, match_2_letras, match100_noname, match_2_letras_noname)

restults_to_file("resultados.txt", resultados)
